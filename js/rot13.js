// Propaganistas provides the Rot13 in the global namespace.
// So it has been adapted here an wrapped for Drupal.
// The inline <script> has been replaced as well.
(function (Drupal, once) {
  'use strict';

  var dummyElement = document.createElement('div');

  function transformRot13(rot13Email) {
    return rot13Email.replace( /[A-Za-z]/g , function(c) {
      return String.fromCharCode( c.charCodeAt(0) + ( c.toUpperCase() <= "M" ? 13 : -13 ) );
    } )
  }

  function init(element) {
    // Remove the css fallback
    var toRemove = element.querySelector('.js-disabled');
    if (toRemove) {
      toRemove.remove();
    }
    // Transform back the rot 13
    var rot13Element = element.querySelector('.js-enabled');
    if (!rot13Element) {
      return;
    }
    var email = transformRot13(rot13Element.textContent);
    var parts = {
      innerhtml: email,
      type: 'plain',
      pre: '',
      post: '',
      query: '',
      q: '',
    };

    for (var attrib in rot13Element.dataset) {
      if (attrib == 'innerhtml') {
        parts[attrib] = transformRot13(rot13Element.dataset[attrib]);
      }
      else {
        parts[attrib] = rot13Element.dataset[attrib];
      }
    }

    if (parts.type == 'mailto') {
      if (parts.query) {
        parts.q = '?';
      }

      // If the innerHTML is HTML, it will have been encoded entities and the
      // actual HTML will get displayed. Make it "real" HTML.
      dummyElement.innerHTML = parts.innerhtml;
      element.outerHTML = `<a ${parts.pre} href="mailto:${email}${parts.q}${parts.query}" ${parts.post}>${dummyElement.textContent}</a>`;
    }
    else {
      element.outerHTML = email;
    }

    element.style.display = 'block';
  }

  Drupal.behaviors.obfuscateRot13 = {
    attach: function (context) {
      var elements = once('boshfpngr-e13', '.boshfpngr-e13', context);
      elements.forEach(function (e) {
        init(e);
      });
    }
  };
})(Drupal, once);
