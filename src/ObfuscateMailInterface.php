<?php

namespace Drupal\obfuscate;

/**
 * Interface ObfuscateMailInterface.
 *
 * @package Drupal\obfuscate
 */
interface ObfuscateMailInterface {

  /**
   * ROT 13 class name of 'obfuscate'.
   *
   * This is not a dependency of ROT 13, it is merely a way
   * to remove hints for spammers.
   */
  const OBFUSCATE_CSS_CLASS = 'boshfpngr';

  /**
   * Returns an obfuscated link from an email address.
   *
   * @param string $email
   *   Email address.
   * @param array $params
   *   Optional parameters to be used by the a tag.
   * @param string $text
   *   Optional text for the a tag innerHtml.
   *
   * @return array
   *   Obfuscated email link render array.
   */
  public function getObfuscatedLink($email, array $params = [], $text = '');

  /**
   * Obfuscates an email address.
   *
   * @param string $email
   *   Email address.
   *
   * @return string
   *   Obfuscated email.
   */
  public function obfuscateEmail($email);

}
