<?php

namespace Drupal\obfuscate;

/**
 * Class ObfuscateMailROT13.
 *
 * Based on the Propaganistas vendor.
 *
 * @see https://packagist.org/packages/propaganistas/email-obfuscator
 * @see https://github.com/Propaganistas/Email-Obfuscator
 *
 * @package Drupal\obfuscate
 */
class ObfuscateMailROT13 implements ObfuscateMailInterface {

  // Safeguard string.
  const SAFEGUARD = '$%$!!$%$';

  /**
   * {@inheritdoc}
   */
  public function getObfuscatedLink($email, $text = '', $extra = []) {
    $build = [
      '#theme' => 'email_rot13_link',
      '#link' => $this->obfuscateEmail($email, $text, $extra),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function obfuscateEmail($email, $text = '', $extra = []) {
    // Propaganistas vendor provides this method as a global function.
    // So it is copied here instead of using Composer.
    // The inline <script> and <noscript> and inline styles
    // for css fallback have been replaced as well.
    // Casting $string to a string allows passing of objects
    // implementing the __toString() magic method.
    $email = (string) $email;

    if ($text) {
      $extra['innerHTML'] = $text;
    }

    array_walk($extra, function(&$value, $key) {
      $escaped = htmlspecialchars($value, ENT_QUOTES);
      if ($key == 'innerHTML') {
        $escaped = str_rot13($escaped);
      }
      $value = "data-{$key}='{$escaped}'";
    });
    $data = implode(' ', $extra);

    $email = trim($email);

    $js = "<span class='js-enabled' {$data}>" . str_rot13($email) . '</span>';
    $nojs = '<span class="js-disabled">' . strrev($email) . '</span>';

    return $js . $nojs;
  }

}
