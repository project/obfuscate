<?php

namespace Drupal\obfuscate\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\Html;
use Drupal\obfuscate\ObfuscateExtractEmailAndLinksTrait;

/**
 * Provides a filter to obfuscate email addresses.
 *
 * The regex patterns are taken from the FilterSpamspan class.
 * See https://www.drupal.org/project/spamspan.
 *
 * @Filter(
 *   id = "obfuscate_mail",
 *   title = @Translation("Email address obfuscation filter"),
 *   description = @Translation("Attempt to hide email addresses from spam-bots."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE
 * )
 */
class ObfuscateMail extends FilterBase {
  use ObfuscateExtractEmailAndLinksTrait;

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Each email address will be obfuscated with the system wide configuration.');
  }

}
