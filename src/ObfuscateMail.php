<?php

namespace Drupal\obfuscate;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class ObfuscateMail.
 *
 * Delegation to the chosen obfuscation method.
 *
 * @package Drupal\obfuscate
 */
class ObfuscateMail implements ObfuscateMailInterface {

  /**
   * Drupal\obfuscate\ObfuscateMailInterface definition.
   *
   * @var \Drupal\obfuscate\ObfuscateMailInterface
   */
  private $obfuscateMailMethod;

  /**
   * ObfuscateMail constructor.
   *
   * Gets the obfuscate mail method from the system wide configuration.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $config = $configFactory->get('obfuscate.settings');
    $method = $config->get('obfuscate.method');
    $this->obfuscateMailMethod = ObfuscateMailFactory::get($method);
  }

  /**
   * {@inheritdoc}
   */
  public function getObfuscatedLink($email, $text = '', $extra = []) {
    return $this->obfuscateMailMethod->getObfuscatedLink($email, $text, $extra);
  }

  /**
   * {@inheritdoc}
   */
  public function obfuscateEmail($email, $text = '') {
    return $this->obfuscateMailMethod->obfuscateEmail($email, $text);
  }

}
