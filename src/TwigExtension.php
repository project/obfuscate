<?php

namespace Drupal\obfuscate;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig extension that wraps obfuscation methods.
 */
class TwigExtension extends AbstractExtension {
  use ObfuscateExtractEmailAndLinksTrait;

  /**
   * @var \Drupal\obfuscate\ObfuscateMail
   */
  protected $obfuscateMail;

  /**
   * Constructor.
   *
   * @param \Drupal\obfuscate\ObfuscateMail $obfuscateEmail
   *   The obfuscate email target.
   */
  public function __construct(ObfuscateMail $obfuscateMail) {
    $this->obfuscateMail = $obfuscateMail;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('obfuscate_mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'obfuscateMail';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFilter('obfuscate', [$this, 'obfuscate']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    $filters = [
      new TwigFilter('obfuscateMail', [$this, 'obfuscateMail']),
    ];
    return $filters;
  }

  /**
   * Replaces email substrings by obfuscated links.
   *
   * The method name is historical - we used to assume the parameter is an
   * email address with no context.
   *
   * @param string $text
   *   Some text being rendered that might contain one or more email addresses.
   *
   * @return array
   *   The text, with any email addresses obfuscated.
   */
  public function obfuscateMail(string $text) {
    return $this->process($text);
  }

  /**
   * Replaces an email string and text by an obfuscated link.
   *
   * @param string $mail
   * @param string $text
   *
   * @return array
   *   The text and email obfuscated as a link.
   */
  public function obfuscate($mail, $text) {
    return $this->obfuscateMail->getObfuscatedLink($mail, [], $text);
  }

}
