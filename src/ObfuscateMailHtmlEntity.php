<?php

namespace Drupal\obfuscate;

/**
 * Class ObfuscateMailHtmlEntity.
 *
 * Obfuscates email addresses by relying on PHP only.
 *
 * @package Drupal\obfuscate
 */
class ObfuscateMailHtmlEntity implements ObfuscateMailInterface {

  /**
   * {@inheritdoc}
   */
  public function getObfuscatedLink($email, $text = '', $extra = []) {

    // Tell search engines to ignore obfuscated uri.
    if (!isset($extra['rel'])) {
      $params['rel'] = 'nofollow';
    }

    $neverEncode = [
      '.',
      '@',
      '+',
      // Don't encode those as not fully supported by IE & Chrome.
    ];

    $urlEncodedEmail = '';
    for ($i = 0; $i < strlen($email); $i++) {
      // Encode 25% of characters.
      if (!in_array($email[$i], $neverEncode) && mt_rand(1, 100) < 25) {
        $charCode = ord($email[$i]);
        $urlEncodedEmail .= '%';
        $urlEncodedEmail .= dechex(($charCode >> 4) & 0xF);
        $urlEncodedEmail .= dechex($charCode & 0xF);
      }
      else {
        $urlEncodedEmail .= $email[$i];
      }
    }

    $obfuscatedEmailUrl = $this->obfuscateEmail('mailto:' . $urlEncodedEmail);
    if (!empty($text)) {
      $innerHtml = $this->obfuscateEmail($text);
    }
    else {
      $innerHtml = $this->obfuscateEmail($email);
    }

    // @todo use twig template to allow override
    $link = '<a href="' . $obfuscatedEmailUrl . '"';
    foreach ($extra as $param => $value) {
      $link .= ' ' . $param . '="' . htmlspecialchars($value) . '"';
    }
    $link .= '>' . $innerHtml . '</a>';
    $build = [
      '#theme' => 'email_link',
      '#link' => $link,
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function obfuscateEmail($email) {
    $alwaysEncode = ['.', ':', '@'];

    $result = '';

    // Encode string using oct and hex character codes.
    $characters = preg_split('//u', $email, -1, PREG_SPLIT_NO_EMPTY);
    foreach ($characters as $character) {
      // Encode 25% of characters including several
      // that always should be encoded.
      if (in_array($character, $alwaysEncode) || mt_rand(1, 100) < 25) {
        if (mt_rand(0, 1)) {
          $result .= '&#' . mb_ord($character) . ';';
        }
        else {
          $result .= '&#x' . dechex(mb_ord($character)) . ';';
        }
      }
      else {
        $result .= $character;
      }
    }

    return $result;
  }

}
